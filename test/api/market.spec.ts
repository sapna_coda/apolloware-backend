import * as chai from 'chai';
import * as sinon from 'sinon';
import { MarketApi } from '../../src/api/utility/market';
import { MarketService } from '../../src/service/market/market.service';
import * as chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);

const event = { }
const stubValue = [{
    id: 1,
    name: 'ERCOT',
    description: 'ERCOT',
},{
    id: 2,
    name: 'WEST',
    description: 'WEST',
}];


describe("Markets Api", function () {
    afterEach(function () {
        sinon.restore();
    });
    describe("getMarkets api", function () {
        it("should retrieve all markets", async function () {
            const stub = sinon.stub(MarketService, "getMarkets").returns(stubValue);
            const marketsData: any = await MarketApi.getMarkets(event);
            const markets: any = JSON.parse(marketsData.body);
            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(markets.length).to.equal(stubValue.length);
            chai.expect(markets[0].id).to.equal(stubValue[0].id);
            chai.expect(markets[1].id).to.equal(stubValue[1].id);
        });
    });
});