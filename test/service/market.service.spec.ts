import * as chai from 'chai';
import * as sinon from 'sinon';
import { Market } from '../../src/dao/models/market.model';
import { MarketService } from '../../src/service/market/market.service';
import * as chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);

const stubValue = [{
    id: 1,
    name: 'ERCOT',
    description: 'ERCOT',
},{
    id: 2,
    name: 'WEST',
    description: 'WEST',
}];

describe("MarketService", function () {

    afterEach(function () {
        sinon.restore();
    });
    describe("getMarkets", function () {

        it("should get all Markets", async function () {
            const stub = sinon.stub(Market, "findAll").returns(stubValue);
            const markets: any = await MarketService.getMarkets();

            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(markets.length).to.equal(stubValue.length);
            chai.expect(markets[0].id).to.equal(stubValue[0].id);
            chai.expect(markets[1].id).to.equal(stubValue[1].id);

            stub.reset();
        });
    });
});