import { Logger } from '../utils/logger.utils';
import { Market } from "../dao/models/market.model"
import { Sequelize } from 'sequelize-typescript';
import { Session } from '../namespaces/session.namespace';
import mysql2 from 'mysql2';

export class DbConfig {
    private static sequelize: Sequelize = null;
    constructor() { }

    /**
   * get DB Connection
   */
    public static async connect() {
        Logger.info('Entered connect:::');
        // let dbSecrets = await DBManager.getSecrets(); //TODO: Integration with Secrets Manager
        const host = process.env['DB_HOST']
            , database = process.env['DATABASE']
            , username = process.env['DB_USERNAME']
            , password = process.env['DB_PASSWORD']
            , port = Number(process.env['PORT'])

        this.sequelize = new Sequelize({
            database: database,
            username: username,
            dialect: 'mysql',
            password: password,
            port: port,
            host: host,
            benchmark: true,
            dialectModule: mysql2,
            dialectOptions: {
                connectTimeout: 60000
            }
        });
        this.init();
        this.sequelize.authenticate().then(function () {
            Logger.debug('Connection has been established successfully.');
        })
            .catch(function (error) {
                Logger.error('Unable to connect to the database:', error);
            });
    }

    public static init() {
        Logger.debug('Entering <init>')
        this.registerModels();
        Session.setValue('SEQUELIZE', this.sequelize);
        Logger.debug('Exiting <init>')
    }

    public static registerModels() {
        Logger.debug('Entering <registerModels>');
        this.sequelize.addModels([
            Market
        ]);
        Logger.debug('Exiting <registerModels>')
    }

   /**
   * Close a DB connection
   */
    public static async closeConnection() {
        Logger.debug('Entering <closeConnection>');
        this.sequelize.close();
        Logger.debug('Exiting <closeConnection>')
    }

}