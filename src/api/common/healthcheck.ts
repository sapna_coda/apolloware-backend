import { Logger } from '../../utils/logger.utils';
import ResponseUtils from '../../utils/response.utils';
import { STATUS } from '../../constants/status.constants';

export class HealthCheckApi {
    static check(event) {
        return new Promise(async (resolve) => {
            try {
                Logger.info('Entering healthcheck', event);
                Logger.info(event.pathParameters);
                const result = "success";
                resolve(ResponseUtils.generateResponse(STATUS.SUCCESS, result))
            } catch (error) {
                Logger.error(error);
                resolve(ResponseUtils.generateResponse(STATUS.ERROR, error));
            }
        });
    }
}

export const check = HealthCheckApi.check;