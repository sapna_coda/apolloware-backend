import { Logger } from '../../utils/logger.utils';
import ResponseUtils from '../../utils/response.utils';
import { STATUS } from '../../constants/status.constants';
import { MarketService } from '../../service/market/market.service';

export class MarketApi {
    static getMarkets(event) {
       
        return new Promise(async (resolve) => {
            try {          
                Logger.info(`Entering <MarketApi.getMarkets> with ${JSON.stringify(event)}`);
                Logger.info(event.pathParameters);
                const markets = await MarketService.getMarkets();
                resolve(ResponseUtils.generateResponse(STATUS.SUCCESS, markets))
            } catch (error) {
                Logger.error(error);
                resolve(ResponseUtils.generateResponse(STATUS.ERROR, error));
            }
        });
    }
}

export const getMarkets = MarketApi.getMarkets;