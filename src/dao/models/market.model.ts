import { Table, PrimaryKey, DataType, Column, Model} from 'sequelize-typescript';


@Table({
  tableName: 'Market'
})
export class Market extends Model<Market> {

  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number;

  @Column(DataType.STRING)
  name: string;

  @Column(DataType.STRING)
  description: string;
}