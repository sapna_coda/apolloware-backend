import { Logger } from '../../utils/logger.utils';
import { Market } from '../../dao/models/market.model';
import { DbConfig } from '../../config/db.config';


export class MarketService {
    public static async getMarkets(): Promise<Market[]> {
        return new Promise(async (resolve, reject) => {
            Logger.info('Entering into <MarketService.getMarkets>');
            try {
                await DbConfig.connect();
                let markets = await Market.findAll();
                resolve(markets);
            } catch (error) {
                Logger.info('Rejecting promise from <MarketService.getMarkets>');
                reject(error);
            } finally {
                await DbConfig.closeConnection();
            }
        });
    }
}