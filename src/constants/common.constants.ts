export const LOG_TIMESTAMP_FORMAT = 'yyyy-mm-dd HH:MM:ss.l Z';
export const HEADER = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
    'Access-Control-Allow-Credentials': true
};