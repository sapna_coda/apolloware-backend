'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    // return queryInterface.dropAllTables().then()
    return queryInterface.createTable('Market', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(function () {
        queryInterface.sequelize.query("insert into Market (name, description) values ('ERCOT', 'ERCOT')");
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Market');
  }
};