# Apolloware Backend


## Getting Started

* Serverless framework 
* NodeJS 12.0 
* MySQL
* Sequelize


## Development


### Running it locally


1. Run the command 'npm install'

2. Set the environment variables - DB_HOST, DB_PORT,  DATABASE, DB_USERNAME, DB_PASSWORD

```
    export DB_HOST=...
    export DB_NAME=...
    export DB_USER_NAME=...
    export DB_PASSWORD=...
```

3. Run the following command to run the api locally

```
    serverless offline start 
```

4. Access the API 

```
    http://localhost:3000/{apiname}
```


### DB migrations

**IMPORTANT NOTE**: Migrations to be run only for local db setup. Please do not run migrations on DEV and upper environments from local, as migrations on DEV and upper environments will be done automatically through pipelines.

The migration files are under migrations folder.

To start migration, run the following command with your local db credentials.

```
    npx sequelize-cli db:migrate --url mysql://<username>:<password>@<dbhost>/<database>
```


